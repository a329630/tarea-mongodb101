// 2) ¿cuantos registros arrojo el comando count?
db.grades.count();
// 800

// 3) encuentra todas las calificaciones del estudiante con el id numero 4
db.find({"student_id":4});
/* { "_id" : ObjectId("50906d7fa3c412bb040eb587"), "student_id" : 4, "type" : "exam", "score" : 87.89071881934647 }
   { "_id" : ObjectId("50906d7fa3c412bb040eb58a"), "student_id" : 4, "type" : "homework", "score" : 28.656451042441 }
   { "_id" : ObjectId("50906d7fa3c412bb040eb588"), "student_id" : 4, "type" : "quiz", "score" : 27.29006335059361 }
   { "_id" : ObjectId("50906d7fa3c412bb040eb589"), "student_id" : 4, "type" : "homework", "score" : 5.244452510818443 }
*/

// 4) ¿cuantos registros hay de tipo exam?
db.grades.count({"type":"exam"});
// 200

// 5) ¿cuantos registros hay de tipo homework?
db.grades.count({"type":"homework"});
// 400

// 6) ¿cuantos registros hay de tipo quiz?
db.grades.count({"type":"quiz"});
// 200

// 7) elimina todas las calificaciones del estudante con el id numero 3
db.grades.updateMany({"student_id":3},{$unset:{score:""}});
// { "acknowledged" : true, "matchedCount" : 4, "modifiedCount" : 4 }

// 8) ¿que estudiantes obtuvieron 75.29561445722392 en una tarea?
db.grades.find({"score":75.29561445722392});
// { "_id" : ObjectId("50906d7fa3c412bb040eb59e"), "student_id" : 9, "type" : "homework", "score" : 75.29561445722392 }
// el estudiante con in numero 9

// 9) actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100 
db.grades.updateOne({"_id":ObjectId("50906d7fa3c412bb040eb591")}, {$set: {"score":100}});
// { "acknowledged" : true, "matchedCount" : 1, "modifiedCount" : 1 }

// 10) a que estudiante pertenece esta calificacion, ojala a mi jajaja
db.grades.find({"score":100});
// { "_id" : ObjectId("50906d7fa3c412bb040eb591"), "student_id" : 6, "type" : "homework", "score" : 100 }
// al estudiante con id numero 6
